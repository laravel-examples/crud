@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <h2>Laravel 7 Crud - {{ $title }}</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('products.index') }}">Back</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <strong>Name</strong>
                <p>{{ $product->name }}</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <strong>Details</strong>
                <p>{{ $product->detail }}</p>
            </div>
        </div>
    </div>
@endsection
