@extends('products.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="float-left">
                <h2>Laravel 7 Crud - {{ $title }}</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('products.index') }}">Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with you input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.update', $product->id) }}" method="post">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name"><strong>Name</strong></label>
            <input class="form-control" type="text" name="name" value="{{ $product->name }}" placeholder="Enter product name">
        </div>
        <div class="form-group">
            <label for="detail"><strong>Details</strong></label>
            <textarea class="form-control" name="detail" placeholder="Enter product details">{{ $product->detail }}</textarea>
        </div>
        <input class="btn btn-primary" type="submit" value="Submit">
    </form>
@endsection
